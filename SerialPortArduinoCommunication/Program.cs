﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Text;
using System.Threading;

namespace SerialPortArduinoCommunication
{
    class Program
    {
        static SerialPort _serialPort;

        public static void Main()
        {
            _serialPort = new SerialPort();
            _serialPort.PortName = "COM9";//Set your board COM
            _serialPort.BaudRate = 9600;
            _serialPort.Open();

            var factory = new ConnectionFactory()
            {
                HostName = "172.20.10.11",
                UserName = "cyril",
                Password = "azerty",
            };

            while (true)
            {
                try
                {

                    double a = Convert.ToDouble(_serialPort.ReadExisting());
                    Sensor sensor = new Sensor {
                        uid = "LumArduino",
                        type = "Luminosity",
                        value = a
                    };
                    List<Sensor> sensors = new List<Sensor>();
                    sensors.Add(sensor);
                    Device device = new Device
                    {
                        uid = "Arduino",
                        sensors = sensors
                    };
                    Console.WriteLine(a);
                    using (var connection = factory.CreateConnection())
                    using (var channel = connection.CreateModel())
                    {
                        //channel.ExchangeDeclare(exchange: "atlantis-exchange",type:, durable:true);
                        channel.QueueDeclare(queue: "helloworld", durable: true, exclusive: false, autoDelete: false, arguments: null);
                        
                        String message = JsonConvert.SerializeObject(device);
                        var body = Encoding.UTF8.GetBytes(message);
                        IBasicProperties props = channel.CreateBasicProperties();
                        props.ContentType = "application/json";
                        channel.BasicPublish(exchange: "atlantis-exchange",
                            routingKey: "",
                            basicProperties: props,
                            body: body);
                        Console.WriteLine(" [x] Sent {0}", body);
                    }
                }
                catch
                {
                    Console.WriteLine("Data reading impossible at the moment");
                }
                Thread.Sleep(1000);
            }
        }
    }

    public class Device
    {
        public string uid;
        public List<Sensor> sensors;

    }

    public class Sensor
    {
        public string uid;
        public string type;
        public double value;
    }
}

    


